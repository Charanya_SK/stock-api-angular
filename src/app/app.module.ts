import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { FormsModule } from '@angular/forms';
import { StockEditComponent } from './stock-edit/stock-edit.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { DashboardUserComponent } from './dashboard-user/dashboard-user.component';
import { VolumeDashboardComponent } from './volume-dashboard/volume-dashboard.component';
import { StatusDashboardComponent } from './status-dashboard/status-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    StockCreateComponent,
    StockListComponent,
    StockEditComponent,
    HomePageComponent,
    LoginSuccessComponent,
    DashboardUserComponent,
    VolumeDashboardComponent,
    StatusDashboardComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
