import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockEditComponent } from './stock-edit/stock-edit.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { DashboardUserComponent } from './dashboard-user/dashboard-user.component';
import { VolumeDashboardComponent } from './volume-dashboard/volume-dashboard.component';
import { StatusDashboardComponent } from './status-dashboard/status-dashboard.component';
const routes: Routes = 
[  
{ path: '', pathMatch: 'full', redirectTo: 'home-page' },
{ path: 'create-stock', component: StockCreateComponent },
{ path: 'stock-list', component: StockListComponent },
{ path: 'stock-edit/:id', component: StockEditComponent },
{ path : 'home-page', component:HomePageComponent},
{ path : 'login-success', component:LoginSuccessComponent},
{ path : 'dashboard-user', component:DashboardUserComponent},
{ path :'volume-dashboard', component: VolumeDashboardComponent},
{ path :'status-dashboard', component: StatusDashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
